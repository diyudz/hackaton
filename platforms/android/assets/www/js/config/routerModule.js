var routerModule = angular.module("routerModule", ['ngRoute']);

routerModule.config(function($routeProvider){
  $routeProvider.when('/app', {
    templateUrl: 'templates/homepage.html'
  })
  .otherwise({
    redirectTo: '/app'
  });
});
